<h1 align="center">
    <img src="https://gitlab.com/An0/SimpleDiscordCrypt/avatar" height="50" width="50">
    SimpleDiscordCrypt
</h1>

Discord message encryption plugin, it gives end-to-end client side encryption for your messages and files with automatic key exchange, works without BetterDiscord

Install it as a [userscript](https://gitlab.com/An0/SimpleDiscordCrypt/raw/master/SimpleDiscordCrypt.user.js) or include the js file somehow else

If you have Discord installed, use the [installer](https://gitlab.com/An0/SimpleDiscordCrypt/raw/master/SimpleDiscordCryptInstaller.ps1) (right click and Run with PowerShell)

Right click on the lock icon to open the menu

If you would like to download an encrypted image, middle click on the image or on "Open original"


ECDH P-521 is used for the key exchange and AES256-CBC for the messages offering the equivalent security of 256-bits

I hope this is actually simple as the name suggests


Here is a link to the original [DiscordCrypt](https://gitlab.com/leogx9r/DiscordCrypt) for BetterDiscord users






If you have any questions come to my [Discord server](https://discord.gg/pbQVQEs)

𝘚𝘪𝘮𝘱𝘭𝘦𝘋𝘪𝘴𝘤𝘰𝘳𝘥𝘊𝘳𝘺𝘱𝘵